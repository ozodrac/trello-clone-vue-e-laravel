FROM php:7.2.6-apache

RUN a2enmod rewrite

# Install Composer
RUN apt-get -y update && \
    apt-get -y install curl nano git vim zip unzip libxml2-dev && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN docker-php-ext-install mysqli pdo pdo_mysql soap

# Configuration for Apache
COPY apache/000-default.conf /etc/apache2/sites-available

# # APC
# RUN pear config-set php_ini /usr/local/etc/php/php.ini
# RUN pecl config-set php_ini /usr/local/etc/php/php.ini


RUN a2enmod rewrite
RUN a2enmod expires
RUN a2enmod mime
RUN a2enmod filter
RUN a2enmod deflate
RUN a2enmod proxy_http
RUN a2enmod headers
RUN a2enmod php7

# Edit PHP INI
RUN echo "memory_limit = 1G" > /usr/local/etc/php/php.ini
RUN echo "upload_max_filesize = 50M" >> /usr/local/etc/php/php.ini
RUN echo "post_max_size = 50M" >> /usr/local/etc/php/php.ini
RUN echo "max_input_time = 60" >> /usr/local/etc/php/php.ini
RUN echo "file_uploads = On" >> /usr/local/etc/php/php.ini
RUN echo "max_execution_time = 300" >> /usr/local/etc/php/php.ini
RUN echo "LimitRequestBody = 100000000" >> /usr/local/etc/php/php.ini
# RUN echo "session.cookie_lifetime = 0" >> /usr/local/etc/php/php.ini
# RUN echo "session.session.gc_maxlifetime = 525600" >> /usr/local/etc/php/php.ini
# RUN echo "session.gc_probability  = 1" >> /usr/local/etc/php/php.ini

# Clean after install
RUN apt-get autoremove -y && apt-get clean all

EXPOSE 80 9000 443 22

# Change website folder rights and upload your website
RUN chown -R www-data:www-data /var/www/html
ADD ./src /var/www/html

# Change working directory
WORKDIR /var/www/html

# Install and update laravel (rebuild into vendor folder)
RUN composer install
RUN composer update


# Create Laravel folders (mandatory)
RUN mkdir -p /var/www/html/bootstrap/cache
RUN mkdir -p /var/www/html/bootstrap/cache

RUN mkdir -p /var/www/html/storage/framework
RUN mkdir -p /var/www/html/storage/framework/sessions
RUN mkdir -p /var/www/html/storage/framework/views
RUN mkdir -p /var/www/html/storage/meta
RUN mkdir -p /var/www/html/storage/cache
RUN mkdir -p /var/www/html/public/storage/uploads/

# Change folder permission
RUN chmod -R 0777 /var/www/html/storage/
RUN chmod -R 0777 /var/www/html/storage/framework
RUN chmod -R 0777 /var/www/html/storage/framework/sessions
RUN chmod -R 0777 /var/www/html/storage/framework/views
RUN chmod -R 0777 /var/www/html/public/storage/uploads/
# Laravel writing rights
RUN chgrp -R www-data /var/www/html/storage /var/www/html/bootstrap/cache
RUN chmod -R ug+rwx /var/www/html/storage /var/www/html/bootstrap/cache

RUN php artisan config:clear
RUN php artisan cache:clear
RUN php artisan config:cache
RUN php artisan storage:link

RUN service apache2 restart